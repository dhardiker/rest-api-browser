package com.atlassian.devrel.servlet;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.Maps;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

public class ToolbarServlet  extends HttpServlet {
    private static final String TOOLBAR_TEMPLATE = "/templates/toolbar.vm";

    private TemplateRenderer renderer;
    private ApplicationProperties applicationProperties;
    private WebInterfaceManager webInterfaceManager;
    private PluginAccessor pluginAccessor;

    public ToolbarServlet(TemplateRenderer renderer, ApplicationProperties applicationProperties,
                          WebInterfaceManager webInterfaceManager, PluginAccessor pluginAccessor) {
        this.webInterfaceManager = webInterfaceManager;
        this.pluginAccessor = pluginAccessor;
        this.renderer = checkNotNull(renderer, "renderer");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, Object> context = Maps.newHashMap();
        Map<String, Object> condition = Maps.newHashMap();

        context.put("navBarItems", webInterfaceManager.getDisplayableItems("dev-toolbar", condition));
        context.put("navBarMenuItems", webInterfaceManager.getDisplayableItems("dev-toolbar-menu", condition));
        context.put("app", applicationProperties);
        context.put("sdkVersion", System.getProperty("atlassian.sdk.version", "3.7 or earlier"));
        context.put("devToolboxVersion",pluginAccessor.getPlugin("com.atlassian.devrel.developer-toolbox-plugin").getPluginInformation().getVersion());

        if (pluginAccessor.isPluginEnabled("com.atlassian.labs.fastdev-plugin")){
            context.put("fastDevVersion",pluginAccessor.getPlugin("com.atlassian.labs.fastdev-plugin").getPluginInformation().getVersion());
        } else {
            context.put("fastDevVersion", "disabled");
        }
        resp.setContentType("text/html;charset=utf-8");
        renderer.render(TOOLBAR_TEMPLATE, context, resp.getWriter());
    }

    private String getAppName() {
        return applicationProperties.getDisplayName().toLowerCase();
    }

}
