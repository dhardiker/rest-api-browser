package com.atlassian.devrel.servlet;

import com.atlassian.devrel.plugin.PlatformComponents;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.Maps;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * Serves the developer toolbox home page.
 */
public class HomepageServlet extends RequiresAdminServlet {

    private static final String HOMEPAGE_TEMPLATE = "/templates/homepage.vm";

    private static final String I18N_AVAILABLE_KEY = "i18nAvailable";
    private static final String I18N_STATE_KEY = "i18nActive";
    private static final String SDK_VERSION_KEY = "sdkVersion";    
    private static final String SPACE_CONTEXT_KEY = "space";
    private static final String SPEAKEASY_AVAILABLE_KEY = "speakeasyAvailable";
    private static final String ACTIVEOBJECTS_AVAILABLE_KEY = "aoAvailable";
    private static final String PLUGIN_DATA_EDITOR_AVAILABLE_KEY = "pdeAvailable";
    private static final String SYSTEM_INFO_LINK_KEY = "systemInfoLink";
    private static final String PLATFORM_COMPONENTS_KEY = "platformComponents";
    
    private final ApplicationProperties applicationProperties;
    private final PluginAccessor pluginAccessor;
    private final PlatformComponents platformComponents;

    public HomepageServlet(UserManager userManager, TemplateRenderer renderer, LoginUriProvider loginUriProvider,
                           ApplicationProperties applicationProperties, PluginAccessor pluginAccessor,
                           PlatformComponents platformComponents) {
        super(userManager, renderer, loginUriProvider);
        this.applicationProperties = applicationProperties;
        this.pluginAccessor = pluginAccessor;
        this.platformComponents = platformComponents;
    }

    @Override
    public Map<String, Object> getContext(HttpServletRequest req) {
        Map<String, Object> context = Maps.newHashMap();
        
        context.put(I18N_AVAILABLE_KEY, isI18nTranslationAvailable());
        if (isI18nTranslationAvailable()) {
            context.put(I18N_STATE_KEY, getI18nState(req));
        }
        
        context.put(SDK_VERSION_KEY, getSdkVersion());
        context.put(SPACE_CONTEXT_KEY, getSpaceNameForProduct());
        context.put(SPEAKEASY_AVAILABLE_KEY, isSpeakeasyAvailable());
        context.put(ACTIVEOBJECTS_AVAILABLE_KEY, isActiveObjectsAvailable());
        context.put(PLUGIN_DATA_EDITOR_AVAILABLE_KEY, isPluginDataEditorAvailable());        
        context.put(SYSTEM_INFO_LINK_KEY, getSystemInfoLink());
        
        context.put(PLATFORM_COMPONENTS_KEY, platformComponents.getPlatformComponents());
        
        return context;
    }

    @Override
    public String getTemplatePath() {
        return HOMEPAGE_TEMPLATE;
    }

    private boolean isI18nTranslationAvailable() {
        return getAppName().equals("jira") || getAppName().equals("confluence");
    }

    private boolean getI18nState(HttpServletRequest req) {
        String appName = getAppName();
        HttpSession session = req.getSession(false);
        if (appName.equals("jira")) {
            return session.getAttribute("com.atlassian.jira.util.i18n.I18nTranslationModeSwitch") != null;
        } else if (appName.equals("confluence")) {
            Object o = session.getAttribute("confluence.i18n.mode");
            if (o != null) {
                return o.getClass().getSimpleName().equals("LightningTranslationMode");
            }
        }

        return false;
    }

    private String getSpaceNameForProduct() {
        String appName = getAppName();
        if (appName.equals("fisheye") || appName.equals("crucible")) {
            return "FECRUDEV";
        } else if (appName.equalsIgnoreCase("confluence")) {
            return "CONFDEV";
        } else {
            return appName.toUpperCase() + "DEV";
        }
    }

    private String getSdkVersion() {
        return System.getProperty("atlassian.sdk.version", "3.7 or earlier");
    }    
    
    private boolean isSpeakeasyAvailable() {
        return pluginAccessor.getEnabledPlugin("com.atlassian.labs.speakeasy-plugin") != null;
    }
    
    private boolean isActiveObjectsAvailable() {
        return pluginAccessor.getEnabledPlugin("com.atlassian.activeobjects.activeobjects-plugin") != null;
    }

    private boolean isPluginDataEditorAvailable() {
        return pluginAccessor.getEnabledPlugin("com.atlassian.plugins.plugin-data-editor") != null;
    }
    
    private String getSystemInfoLink() {
        String url = "";
        String appName = getAppName();
        if (appName.equals("jira")) {
            url = "/secure/admin/ViewSystemInfo.jspa";
        } else if (appName.equals("confluence") || appName.equals("bamboo")) {
            url = "/admin/systeminfo.action";
        } else if (appName.equals("fisheye") || appName.equals("crucible")) {
            url = "/admin/sysinfo.do";
        } else if (appName.equals("crowd")) {
            // can't get right now
        }

        return url;
    }
    
    private String getAppName() {
        return applicationProperties.getDisplayName().toLowerCase();
    }
}
