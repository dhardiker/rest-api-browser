// t.js (https://github.com/jasonmoo/t.js): tiny javascript templating
(function(){function f(a){this.t=a}function j(a,b){for(var c=b.split(".");c.length;){if(!(c[0]in a))return!1;a=a[c.shift()]}return a}function d(a,b){return a.replace(h,function(c,a,e,f,k,h,i,l){var c=j(b,f),a="",g;if(!c)return"!"==e?d(k,b):i?d(l,b):"";if(!e)return d(h,b);if("@"==e){for(g in c)c.hasOwnProperty(g)&&(b._key=g,b._val=c[g],a+=d(k,b));delete b._key;delete b._val;return a}}).replace(i,function(a,d,e){return(a=j(b,e))||0===a?"%"==d?(new Option(a)).innerHTML.replace(/"/g,"&quot;"):a:""})}
var h=/\{\{(([@!]?)(.+?))\}\}(([\s\S]+?)(\{\{:\1\}\}([\s\S]+?))?)\{\{\/\1\}\}/g,i=/\{\{([=%])(.+?)\}\}/g;f.prototype.render=function(a){return d(this.t,a)};window.t=f})();

AJS.$(function($){

    // Don't bother running this if it's IE
    if(!$.support.boxModel || !$.support.noCloneChecked) {
        return false;
    }

    function getQueryParamByName(name){
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.search);
        if(results == null)
            return false;
        else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    // Global dtToolbar object stores helper props/funcs for others who want to plug into the toolbar
    window.dtToolbar = {};
    dtToolbar.baseUrl = "%%BASEURL%%"; dtToolbar.contextPath = dtToolbar.baseUrl.split('/').length > 3 ? "/" + dtToolbar.baseUrl.split('/') [3] : "/";

    function toggleToolbar(){
        if($('.dt-toolbar').hasClass('dt-expando-expanded')) {
            $('.dt-toolbar').removeClass('dt-expando-expanded');
            localStorage.setItem('dev.toolbar.enabled',false);
        } else {
            $('.dt-toolbar').addClass('dt-expando-expanded');
            localStorage.setItem('dev.toolbar.enabled',true);
        }
    }

    $('body').delegate('.dt-expando','click',toggleToolbar);

//    AJS.whenIType('dt').execute(toggleToolbar);

    function getUri(){
        return window.location.protocol + '//' + window.location.pathname;
    }

    dtToolbar.init = function(d){
        $('body').append(d);
        if(localStorage.getItem('dev.toolbar.enabled') === "true") {
            toggleToolbar();
        }

        $('body').delegate('#dt-highlight-i18n','click',function(){
            var i18nParam = getQueryParamByName('i18ntranslate');
            if(!i18nParam) {
                window.location = window.location.search ? getUri() + window.location.search + "&i18ntranslate=on" + window.location.hash : getUri() + "?i18ntranslate=on" + window.location.hash;
            } else {
                if(i18nParam === "on") {
                    window.location = window.location.href.replace("i18ntranslate=on","i18ntranslate=off");
                }else{
                    window.location = window.location.href.replace("i18ntranslate=off","i18ntranslate=on");
                }
            }
        });
        $('body').delegate('#dt-stash-web-fragments','click',function(){
            var paramsOn = /web\.panels&web\.items&web\.sections/g.test(window.location.search);
            if(!paramsOn) {
                window.location = window.location.search ? getUri() + window.location.search + "&web.panels&web.items&web.sections" + window.location.hash : getUri() + "?web.panels&web.items&web.sections" + window.location.hash;
            } else {
                if(/\?web\.panels&web\.items&web\.sections&/.test(window.location.search)) {
                    window.location = window.location.href.replace(/\?web\.panels&web\.items&web\.sections&/,"?");
                } else {
                    window.location = window.location.href.replace(/[&\?]web\.panels&web\.items&web\.sections/,"");
                }
            }
        });
    }

    $.ajax({
        type: 'GET',
        url: dtToolbar.contextPath + "/plugins/servlet/dev-toolbar",
        dataType: 'html',
        success: dtToolbar.init
    });

    function resetMenus(){
        $('.dt-menu.dt-expand').removeClass('dt-expand');
        $('.dt-search-results').removeClass('dt-expand');

        // DEVBOX-5 hack to fix the flyover menu being disabled when live reload is on
        $('.dt-reload-on').attr('style','');
    }

    $('html').bind('click',function(e){
        resetMenus();
    });

    function toggleDTMenu(e){
        // DEVBOX-5 hack to fix the flyover menu being disabled when live reload is on
        if(!$(e.currentTarget).hasClass('dt-brand')) return;

        var $this = $('.dt-toolbox-btn');
        if($this.find('.dt-menu').hasClass('dt-expand')){
            $this.find('.dt-menu').removeClass('dt-expand');
            // $('html').trigger('dt:toolbox-menu:click');
        }else{
            $('.dt-search-results').removeClass('dt-expand');
            $this.find('.dt-menu').addClass('dt-expand');
            // DEVBOX-5 hack to fix the flyover menu being disabled when live reload is on
            $('.dt-reload-on').attr('style','-webkit-animation-name: none; -moz-animation-name: none;');
        }
        e.stopPropagation();
    }

    $('body').delegate('.dt-brand','click',toggleDTMenu);

    // handler for search box input
    $('body').delegate('.dt-search-box','keyup focus click',function(e){
        if(/focusin/.test(e.type)){
            $(this).trigger('select');
        }
        $('.dt-menu.dt-expand').removeClass('dt-expand');
        var query = $(this).val();
        $('.dt-search-query').text(query);

        // DEVBOX-5 hack to fix the flyover menu being disabled when live reload is on
        $('.dt-reload-on').attr('style','-webkit-animation-name: none; -moz-animation-name: none;');
        if(query.length > 2 && /keyup|focusin/.test(e.type)) {
            $('.dt-search-results-list').css({
                'max-height': $(window).height() / 2
            });
            if(/keyup/.test(e.type)) {
                performSearch('doc',query);
            }
        }
        e.stopPropagation();
    });

    function performSearch(scope,query){
        $('.dt-search-scopes .dt-tab-active').removeClass('dt-tab-active');
        $('a[data-scope='+scope+']').addClass('dt-tab-active');
        var url, callbackName;
        if (scope === "doc"){
            url = "https://developer.atlassian.com/rest/dac/2/search?query=" + encodeURIComponent(query) + "&type=all&pageSize=10&startIndex=0"
        } else if(scope === "api") {
            url = "https://developer.atlassian.com/rest/dac/2/google/search/all?q=" + encodeURIComponent(query) + "&num=10&start=0"
        } else if(scope === "answers") {
            url = "https://developer.atlassian.com/rest/answers/1.0/search?query=" + encodeURIComponent(query);
        }
        searchInProgress(true);
        $.ajax({
            url: url,
            type: "get",
            dataType: "jsonp",
            processData:false,
            jsonp: 'jsonp-callback',
            success: function(r){
                searchInProgress(false);
                var template,data;
                if(scope==="doc"){
                    template = new t($('#dt-search-rslt-dac-tmpl').html());
                    $('.dt-search-results-list').html(template.render(r));
                } else if (scope==="api") {
                    template = new t($('#dt-search-rslt-google-tmpl').html());
                    $('.dt-search-results-list').html(template.render(r));
                } else if (scope==="answers"){
                    data = { items: r }
                    template = new t($('#dt-search-rslt-answers-tmpl').html());
                    $('.dt-search-results-list').html(template.render(data));
                }
                $('.dt-search-results').addClass('dt-expand');
                $('.dt-search-results-list').css({
                    'max-height': $(window).height() / 2
                });
            }
        });
    }

    function searchInProgress(on){
        var loader = $('.dt-loader');
        if(on){
            loader.removeClass('dt-hidden');
            $('.dt-search-results-list').css('opacity','.2');
        } else {
            loader.addClass('dt-hidden');
            $('.dt-search-results-list').css('opacity','1');
        }
    }

    $('body').delegate('.dt-search-scope','click',function(e){
        var $this = $(this);
        var query = $('.dt-search-box').val();
        var scope = $this.data('scope');
        performSearch(scope,query);
        e.stopPropagation();
        return false;
    });
});